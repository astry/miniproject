
#include<stdio.h> 
#include<string.h> 
#include<stdlib.h> 
#include<unistd.h> 
#include<sys/types.h> 
#include<sys/wait.h> 
#include<readline/readline.h> 
#include<readline/history.h> 

#define MAXCOM 1000 
#define MAXLIST 100 
#define clear() printf("\033[H\033[J") 

void init_shell() 
{ 
	clear(); 
	printf("\n\n\n\n******************"
		"************************"); 
	printf("\n\n\n\t****WELCOME****"); 
	 
	printf("\n\n\n\n*******************"
		"***********************"); 
	char* username = getenv("USER"); 
	printf("\n\n\n Have a nice time working here : @%s", username); 
	printf("\n"); 
	sleep(3); 
	clear(); 
} 

int takeInput(char* str) 
{ 
	char* buf; 

	buf = readline("\n>>> "); 
	if (strlen(buf) != 0) { 
		add_history(buf); 
		strcpy(str, buf); 
		return 0; 
	} else { 
		return 1; 
	} 
} 

void printDir() 
{ 
	char cwd[1024]; 
	getcwd(cwd, sizeof(cwd)); 
	printf("\nDir: %s", cwd); 
} 

//  system command executed.. 
void execArgs(char** parsed) 
{ 
	pid_t pid = fork(); 

	if (pid == -1) { 
		printf("\n erreur :( no forking child"); 
		return; 
	} else if (pid == 0) { 
		if (execvp(parsed[0], parsed) < 0) { 
			printf("We are working on this command.. !!"); 
		} 
		exit(0); 
	} else { 
		// waiting for child  
		wait(NULL); 
		return; 
	} 
} 

// piped system commands is executed 
void execArgsPiped(char** parsed, char** parsedpipe) 
{ 
	// 0 is read end, 1 is write end 
	int pipefd[2]; 
	pid_t p1, p2; 

	if (pipe(pipefd) < 0) { 
		printf("\n :( Pipe could not be initialized"); 
		return; 
	} 
	p1 = fork(); 
	if (p1 < 0) { 
		printf("\n Sorry Could not fork"); 
		return; 
	} 

	if (p1 == 0) { 
		// Child 1 executing.. 
		// It only needs to write at the write end 
		close(pipefd[0]); 
		dup2(pipefd[1], STDOUT_FILENO); 
		close(pipefd[1]); 

		if (execvp(parsed[0], parsed) < 0) { 
			printf("\nCould not execute command 1.."); 
			exit(0); 
		} 
	} else { 
		// Parent executing 
		p2 = fork(); 

		if (p2 < 0) { 
			printf("\nCould not fork"); 
			return; 
		} 

		// Child 2 executing.. 
		// It only needs to read at the read end 
		if (p2 == 0) { 
			close(pipefd[1]); 
			dup2(pipefd[0], STDIN_FILENO); 
			close(pipefd[0]); 
			if (execvp(parsedpipe[0], parsedpipe) < 0) { 
				printf("\nCould not execute command 2.."); 
				exit(0); 
			} 
		} else { 
			// parent executing, waiting for two children 
			wait(NULL); 
			wait(NULL); 
		} 
	} 
} 


void openHelp() 
{ 
	puts("\n***WELCOME TO MY SHELL HELP**"
		"\nList of Commands supported:"
		"\n>changedirectory"
		"\n>list"
		"\n>dir"
		"\n>hello"
		"\n>exit"); 

	return; 
} 

// Function to execute builtin commands 
int ownCmdHandler(char** parsed) 
{ 
	int NoOfOwnCmds =7, i, switchOwnArg = 0; 
	char* ListOfOwnCmds[NoOfOwnCmds];
	char* username = getenv("USER"); 
int r;

	ListOfOwnCmds[0] = "exit"; 
	ListOfOwnCmds[1] = "changedirectory"; 
	ListOfOwnCmds[2] = "help"; 
	ListOfOwnCmds[3] = "list";
	ListOfOwnCmds[4] = "hello";
	ListOfOwnCmds[5] = "alias";
	ListOfOwnCmds[6] = "c";

	for (i = 0; i < NoOfOwnCmds; i++) { 
		if (strcmp(parsed[0], ListOfOwnCmds[i]) == 0) { 
			switchOwnArg = i + 1; 
			break; 
		} 
	} 

	switch (switchOwnArg) { 
	case 1: 
		printf("\nSee you soon :) \n"); 
		exit(0); 
	case 2: 
		chdir(parsed[1]); 
		return 1; 
	case 3: 
		openHelp(); 
		return 1; 
	case 4: 
	        r=system("ls -l");
		return r; 
	case 5:
		
		printf("\n Hello to you too @%s",username);
		return 1;
	case 6:
		printf("\n alias created !");
        	return 1;
	case 7:
		r=system("clear");
		return r;
	default: 
		break; 
	} 

	return 0; 
} 

//finding pipe 
int parsePipe(char* str, char** strpiped) 
{ 
	int i; 
	for (i = 0; i < 2; i++) { 
		strpiped[i] = strsep(&str, "|"); 
		if (strpiped[i] == NULL) 
			break; 
	} 

	if (strpiped[1] == NULL) 
		return 0; // returns zero if no pipe is found. 
	else { 
		return 1; 
	} 
} 

// parsing command words 
void parseSpace(char* str, char** parsed) 
{ 
	int i; 

	for (i = 0; i < MAXLIST; i++) { 
		parsed[i] = strsep(&str, " "); 

		if (parsed[i] == NULL) 
			break; 
		if (strlen(parsed[i]) == 0) 
			i--; 
	} 
} 

int processString(char* str, char** parsed, char** parsedpipe) 
{ 

	char* strpiped[2]; 
	int piped = 0; 

	piped = parsePipe(str, strpiped); 

	if (piped) { 
		parseSpace(strpiped[0], parsed); 
		parseSpace(strpiped[1], parsedpipe); 

	} else { 

		parseSpace(str, parsed); 
	} 

	if (ownCmdHandler(parsed)) 
		return 0; 
	else
		return 1 + piped; 
} 

int main() 
{ 
	char inputString[MAXCOM], *parsedArgs[MAXLIST]; 
	char* parsedArgsPiped[MAXLIST]; 
	int execFlag = 0; 
	init_shell(); 

	char* alias=(char*)malloc(10*sizeof(char));
	char* deletealias=(char*)malloc(10*sizeof(char));
	alias="makealias";
	deletealias="deletealias";


	while (1) { 
		// print shell line 
		printDir(); 
		// take input 
		if (takeInput(inputString)) 
			continue; 
		// process 
		char* aliastomake=(char*)malloc(10*sizeof(char))
		execFlag = processString(inputString, 
		parsedArgs, parsedArgsPiped); 
		// execflag returns zero if there is no command 
		// or it is a builtin command, 
		// 1 if it is a simple command 
		// 2 if it is including a pipe. 

		// execute 
		FILE *fp;
DIR*p;
struct dirent *d;
int i;
char* inputvalue=takeInput();
if(strstr(inputvalue,cd)!=NULL && strstr(inputvalue,alias)==NULL){
    path=strndup(inputvalue+strlen(cd)+1,500);
    changedirectory(path);
}
else if(strstr(inputvalue,ls)!=NULL && strstr(inputvalue,">")==NULL && strstr(inputvalue,alias)==NULL){
    dirname=strndup(inputvalue+strlen(ls)+1,500);
    p=opendir(dirname);
    if(p==NULL)
    {
    printf("Cannot find directory");
    }
    else{while(d=readdir(p))
    printf("%s\n",d->d_name);
    }}
else if(strstr(inputvalue,clear)!=NULL && strstr(inputvalue,alias)==NULL)
    {
        clear();
    }
else if(strstr(inputvalue,exitt)!=NULL && strstr(inputvalue,alias)==NULL)
    {
        printf("Good Bye! :)\n");
        exit(0);
    }
else if(strstr(inputvalue,touch)!=NULL && strstr(inputvalue,alias)==NULL)
    {
        filetocreate=strndup(inputvalue+strlen(touch)+1,500);
        fp=fopen(filetocreate,"w");
        fclose(fp);
    }
else if(strstr(inputvalue,">")!=NULL && strstr(inputvalue,alias)==NULL){
    if (strstr(inputvalue,ls)!=NULL){
        dirname=strndup(inputvalue+strlen(ls)+1,1);
                i=1;
                while (strstr(dirname,">")==NULL)
                {
                    dirname=strndup(inputvalue+strlen(ls)+1,1+i);
                    i++;
                }
                dirname=strndup(inputvalue+strlen(ls)+1,i-2);
                filetocreate=strndup(inputvalue+strlen(ls)+1+i+1,500);
                fp = fopen(filetocreate, "w");
                p=opendir(dirname);
                if(p==NULL)
                {
                    printf("Cannot find directory");
                }
                else{while(d=readdir(p))
                fprintf(fp,"%s\n",d->d_name);
                }
                fclose(fp);
                printf("Output redirected to %s",filetocreate);
    }         
    }
else if(strstr(inputvalue,exect)!=NULL && strstr(inputvalue,alias)==NULL){
    filename=strndup(inputvalue+strlen(exect)+1,500);
    fp = fopen(filename, "r");
    struct stat stp = { 0 };
    stat(filename, &stp);
    int filesize = stp.st_size;
    message = (char *) malloc(sizeof(char) * filesize);
    if (fread(message, 1, filesize - 1, fp) == -1) {
        printf("\nerror in reading\n");
        fclose(fp);
        free(message);
    }
    strcpy(message, strtok(message , ";"));
    if(strstr(message,cd)!=NULL){
    path=strndup(message+strlen(cd)+1,500);
    changedirectory(path);}
    for (int i = 0; i < 4; i++)
    {
        strcpy(message, strtok(NULL , ";"));
        if(strstr(message,cd)!=NULL){
        path=strndup(message+strlen(cd)+1,500);
        changedirectory(path);}
        else if(strstr(message,touch)!=NULL)
        {
        filetocreate=strndup(message+strlen(touch)+1,500);
        fp=fopen(filetocreate,"w");
        fclose(fp);
        }
        else if(strstr(message,ls)!=NULL && strstr(message,">")==NULL){
        dirname=strndup(message+strlen(ls)+1,500);
        p=opendir(dirname);
        if(p==NULL)
        {
        printf("Cannot find directory");
        }
        else{while(d=readdir(p))
        printf("%s\n",d->d_name);
        }}
        else if(strstr(message,">")!=NULL){
        if (strstr(message,ls)!=NULL){
            dirname=strndup(message+strlen(ls)+1,1);
                    i=1;
                    while (strstr(dirname,">")==NULL)
                    {
                        dirname=strndup(message+strlen(ls)+1,1+i);
                        i++;
                    }
                    dirname=strndup(message+strlen(ls)+1,i-2);
                    filetocreate=strndup(message+strlen(ls)+1+i+1,500);
                    fp = fopen(filetocreate, "w");
                    p=opendir(dirname);
                    if(p==NULL)
                    {
                        printf("Cannot find directory");
                    }
                    else{while(d=readdir(p))
                    fprintf(fp,"%s\n",d->d_name);
                    }
                    fclose(fp);
                    printf("Output redirected to %s",filetocreate);
        }         
        }
    }    
    
}
else if(strstr(inputvalue,alias)!=NULL && strstr(inputvalue,"=")!=NULL)
    {
        aliastomake=strndup(inputvalue+10,1);
        
        i=1;
                    
                    while (strstr(aliastomake,"=")==NULL)
                    {
                        aliastomake=strndup(inputvalue+10,1+i);
                        i++;
                    }
                    newcommand=strndup(inputvalue+10+i+1,500);
                    if(strstr(aliastomake,cd)!=NULL)
                    {
                        cd=newcommand;
                    }
                    else if(strstr(aliastomake,ls)!=NULL)
                    {
                        ls=newcommand;
                    }
                    else if(strstr(aliastomake,touch)!=NULL)
                    {
                        touch=newcommand;
                    }
                    else if(strstr(aliastomake,clear)!=NULL)
                    {
                        clear=newcommand;
                    }
                    else if(strstr(aliastomake,exitt)!=NULL)
                    {
                        exitt=newcommand;
                    }
                    else if(strstr(aliastomake,exect)!=NULL)
                    {
                        exect=newcommand;
                    }
                    else if(strstr(aliastomake,mkadir)!=NULL)
                    {
                        mkadir=newcommand;
                    }
                    else{
                        printf("\nCommand not found to make an alias for!");
                    }
    } 
else if(strstr(inputvalue,deletealias)!=NULL){
cd="goto";
ls="showfiles";
clear="clearterminal";
exitt="quitterminal";
touch="createfile";
exect="executefile";
alias="makealias";
mkadir="createdirectory";
} 
else if(strstr(inputvalue,mkadir)!=NULL && strstr(inputvalue,alias)==NULL && strstr(inputvalue,"|")==NULL){
    directorytocreate=strndup(inputvalue+strlen(mkadir)+1,500);
    

    if (stat(directorytocreate, &st) == -1) {
        mkdir(directorytocreate, 0777);
    }
    else{
        printf("\nDirectory already exists!");
    }
}
else if(strstr(inputvalue,"|")!=NULL && strstr(inputvalue,alias)==NULL){
    if (strstr(inputvalue,mkadir)!=NULL){
        directorytocreate=strndup(inputvalue+strlen(mkadir)+1,1);
                i=1;
                while (strstr(directorytocreate,"|")==NULL)
                {
                    directorytocreate=strndup(inputvalue+strlen(mkadir)+1,1+i);
                    i++;
                }
                directorytocreate=strndup(inputvalue+strlen(mkadir)+1,i-2);
                if (stat(directorytocreate, &st) == -1) {
                    mkdir(directorytocreate, 0777);
                }
                else{
                    printf("\nDirectory already exists!\n");
                }
                arg=strndup(inputvalue+strlen(mkadir)+1+i+1,500);
                if (strstr(arg,"enter")!=NULL)
                {
                    getcwd(cwd, sizeof(cwd));
                    strcat(cwd,"/");
                    strcat(cwd,directorytocreate);
                    changedirectory(cwd);
                }
                else
                {
                    printf("\nArgument not detectable");
                }
                
    }         
    }
		if (execFlag == 1) 
			execArgs(parsedArgs); 

		if (execFlag == 2) 
			execArgsPiped(parsedArgs, parsedArgsPiped); 
	} 
	return 0; 
} 


